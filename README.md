# Spletna aplikacija Elovec

## Tehnologija programske opreme 2019/20


Aplikacija je dostopna na spletnem naslovu: http://elovec.cf

Za testiranje aplikacije se lahko vanjo prijavite z računi:

uporabnik: 'uporabnik@test.com' (družina 1)

uporabnik: 'uporabnik2@test.com' (družina 2)

admin: 'admin@test.com' (admin družine 1)

geslo za vse: 'Banana 123'


### Frontend

Napisan v HTML, CSS, Vue, Jquery.

Stoji na https://www.000webhost.com